/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

/**
 *
 * @author March
 */
public class Frame {
    private String frameNumber;
    private boolean available;

    public Frame(String frameNumber, boolean available) {
        this.frameNumber = frameNumber;
        this.available = available;
    }

    public String getFrameNumber() {
        return frameNumber;
    }


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    
    
    
}
