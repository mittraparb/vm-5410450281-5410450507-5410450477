/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import tools.Tools;

/**
 *
 * @author March PRAlgorithm - Page Replacement Algorithm
 */
public abstract class PRAlgorithm {

    protected ArrayList<Integer> processes;
    
    protected ArrayList<Fields> list;
    protected ArrayList<Frame> physicalMemoryFrame;
    protected Queue queue;
    protected int totalPage;
    protected byte arcBit;
    protected long pageFault;
    protected long diskRef;
    protected int c_instruction;
    protected byte replacementPolicy;
    protected double memorySize;
    protected int pageSize;
    protected byte rp;
    protected int diskRead,diskWrite;
    public PRAlgorithm(byte arcBit, int mSize, int pSize, byte rp) throws Exception {
        this.arcBit = arcBit;
        //Convert kilobyte to byte
        this.memorySize = mSize * Math.pow(2, 10);
        this.pageSize = pSize;
        this.pageFault = 0;
        this.diskRef = 0;
        this.c_instruction = 0;
        this.diskRead = 0;
        this.diskWrite = 0;
        this.rp = rp;
        calculateTotalPage();
        init();
    }

    private void init() throws Exception {
        processes = new ArrayList<>();
        queue = new LinkedList();
        list = new ArrayList<>();
        physicalMemoryFrame = new ArrayList<>();
        String bin;
        for (int i = 0; i < totalPage; i++) {
            bin = Tools.decimalToBinaryString(i);
            bin = Tools.optimizeBinaryString(arcBit, bin);
            //System.out.print("Covert Framenumber "+bin +" ");
            bin = bin.substring(getOffsetBitLength(), bin.length());
            //System.out.print(bin+" "+bin.length()+"\n");
            physicalMemoryFrame.add(new Frame(bin, true));
        }
    }

    private void calculateTotalPage() throws Exception {
        //double n = arcBit - (int)Tools.log2(pageSize);
        //totalPage = (int)Math.pow(2, n);
        double n = memorySize / pageSize;
        if (n < 1) {
            throw new Exception("Require more memory size.");
            
        }
        
        totalPage = (int) (Math.ceil(n));
    }

    protected void clearAllReferenceBit() {
        for (Object obj : queue) {
            ((Fields)obj).setReferenced((byte) 0);    
        }
        c_instruction = 0;
    }
    
    public int getPageNumberBitLength() throws Exception{
        int offsetBitSize = (int) Math.ceil(Tools.log2(pageSize));
        return arcBit - offsetBitSize;
    }
    
    public int getOffsetBitLength() throws Exception{
        return (int) Math.ceil(Tools.log2(pageSize));
    }
    
    protected String getPageNumberBit(String addr) throws Exception {
        if (addr.equals("")) {
            throw new Exception("Error: param is null");
        }
        String bin = Tools.optimizeBinaryString(arcBit, addr);
        int offsetBitSize = (int) Math.ceil(Tools.log2(pageSize));
        int pageNumberBitSize = arcBit - offsetBitSize;
        return bin.substring(0, pageNumberBitSize - 2);
    }

    protected String getOffsetBit(String addr) throws Exception {
        if (addr.equals("")) {
            throw new Exception("Error: param is null");
        }
        String bin = Tools.optimizeBinaryString(arcBit, addr);
        int offsetBitSize = (int) Math.ceil(Tools.log2(pageSize));
        int pageNumberBitSize = arcBit - offsetBitSize;
        return bin.substring(pageNumberBitSize - 2, bin.length());
    }

    protected void mappingFrameNumber(int pageNo) {
        //pageTable.get(pageNo).setFrameNumber(getFrameNumber());
    }

    protected String getFrameNumber() {
        for (Frame frame : physicalMemoryFrame) {
            if (frame.isAvailable()) {
                frame.setAvailable(false);
                return frame.getFrameNumber();
            }
        }
        return null;
    }

    protected void release(String frameNumber) throws Exception {
        for (Frame frame : physicalMemoryFrame) {
            if (frame.getFrameNumber().equals(frameNumber)) {
                if (!frame.isAvailable()) {
                    frame.setAvailable(true);
                    return;
                }
            }
        }
    }

    public long getPageFault() {
        return pageFault;
    }

    public long getDiskRef() {
        return diskRef;
    }
    
    public void debugProcessList() {
        //System.out.println("DEBUG PROCESS LIST");
        for(Integer i : processes) {
            System.out.println(i);
        }
    }
    public int getTotalProcess() {
        return processes.size();
    }

    public int getDiskRead() {
        return diskRead;
    }

    public int getDiskWrite() {
        return diskWrite;
    }
    
    protected void addProcess(int pid) {
        //System.out.print("ADD PID "+pid);
        if(processes.isEmpty()) {
            processes.add(pid);
           // System.out.print(" --ADDED\n");
        }
        else if(!processes.contains(pid)) {
            processes.add(pid);  
            //System.out.print(" --ADDED\n");
        }
    }
    public boolean search(int pid, String addr, String action) throws Exception {
       // System.out.println("Search param :" + addr + " DEC:" + Tools.binaryStringToDecimal(addr));
        String pageNumber = getPageNumberBit(addr);
        addProcess(pid);
        for (Object obj : queue) {
            Fields fields = (Fields) obj;
            //System.out.print("compare " + pageNumber + " = " + fields.getPageNumber());
            if (pageNumber.equals(fields.getPageNumber())) {
                //System.out.print(" --FOUND\n");
                if(action.equalsIgnoreCase("W")) {
                    fields.setDirtyBit((byte)1);
                }
                else if(action.equalsIgnoreCase("R")) {
                    fields.setDirtyBit((byte)0);
                }
                fields.setReferenced((byte)1);
                return true;
            }
        }
        //System.out.print(" --NOT FOUND\n");

        //Page Fault
        diskRef++;
        pageFault++;
        return false;
    }

    public void debug() {
        System.out.println("DEBUG : total page " + totalPage);
        for (Object obj : queue) {
            Fields fields = (Fields) obj;
            System.out.println(fields.getPageNumber());
        }
    }

    public void debugMem() {
        System.out.println("DEBUG Memory Frame");
        for (Frame frame : physicalMemoryFrame) {
            System.out.println("Frame: "+frame.getFrameNumber() + " " + frame.isAvailable());
        }
    }

    public void debugPage() {
        System.out.println("DEBUG Queue");
        for (Object obj : queue) {
            Fields f = (Fields) obj;
            System.out.println(f.getPid() + " " + f.getPageNumber() + " " + f.getFrameNumber());
        }
    }

    public boolean add(int pid, String logicalAddr, String mod) throws Exception {
        if (c_instruction == 200) {
            clearAllReferenceBit();
        }

        if (queue.size() == totalPage) {
            if(mod.equalsIgnoreCase("W"))
                diskWrite++;
            else
                diskRead++;
            //auto replace
            //replace(pid, logicalAddr, mod);
            return false;
        } else {
            
            String frameNumber = getFrameNumber();
            if(frameNumber == null) System.out.println("Found it!!!");
            //System.out.println("DEBUG PRA(ADD)");
            //System.out.println(pid + " " + getPageNumberBit(logicalAddr) + " " + frameNumber);
            //Fields(int pid, String pagdNumber,boolean caching, byte referenced, byte dirtyBit, byte protection, byte present, String frameNumber)
            if (mod.equalsIgnoreCase("W")) {
                Fields f = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 1, (byte) 1, (byte) 2, (byte) 1, frameNumber);
                queue.add(f); // FIFO SEC
            } else if (mod.equalsIgnoreCase("R")) {
                Fields f = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 1, (byte) 0, (byte) 2, (byte) 1, frameNumber);
                queue.add(f); // FIFO SEC
            }
            c_instruction++;
            return true;
        }
    }

    public abstract void replace(int pid, String logicalAddr, String mod) throws Exception;
}
