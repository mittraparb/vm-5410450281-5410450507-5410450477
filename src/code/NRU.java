/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author fahfoii
 */
public class NRU extends PRAlgorithm{

    public NRU(byte arcBit, int mSize, int pSize, byte rp) throws Exception {
        super(arcBit, mSize, pSize, rp);
    }
    
    @Override
    public void replace(int pid, String logicalAddr, String mod) throws Exception {
        //System.out.println("SEC REPLACE");
        //local
        if(rp == 0) {
            
        }
        //global
        else {
            Fields temp = getFieldInLowestGroup();
            //check dirty bit
            if(temp.getDirtyBit() == 1) {
                diskRef++;
                diskWrite++;
            }
            
            
            //System.out.println(getFrameNumber());
            release(temp.getFrameNumber());
            
            String frameNumber = getFrameNumber();
            //System.out.println("DEBUG FIFO(REPLACE)");
            //System.out.println(pid + " " + getPageNumberBit(logicalAddr) + " " + frameNumber);
            //Fields(int pid, String pagdNumber,boolean caching, byte referenced, byte dirtyBit, byte protection, byte present, String frameNumber)
            
            Fields newFields = null;
            if (mod.equalsIgnoreCase("W")) {
                newFields = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 0, (byte) 1, (byte) 2, (byte) 1, frameNumber);
            } else if (mod.equalsIgnoreCase("R")) {
                newFields = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 0, (byte) 0, (byte) 2, (byte) 1, frameNumber);
            }
            //System.out.println(newFields);
            list.remove(temp);
            list.add(newFields);
            pageFault++;
            c_instruction++;
        }
    }
    private Fields getFieldInLowestGroup(){
        Fields temp = null;
        int lowest = 4;
        for (Fields f : list) {
            //lowest
            if(f.getReferenced() == 0 && f.getDirtyBit() == 0) {
                return f;
            }
            else if(f.getReferenced() == 0 && f.getDirtyBit() == 1) {
                if(lowest > 1) {
                    lowest = 1;
                    temp = f;
                }
            }
            else if(f.getReferenced() == 1 && f.getDirtyBit() == 0) {
                if(lowest > 2) {
                    lowest = 2;
                    temp = f;
                }
            }
            else if(f.getReferenced() == 1 && f.getDirtyBit() == 1) {
                if(lowest > 3) {
                    lowest = 3;
                    temp = f;
                }
            }
        }
        return temp;
    }
    @Override
    public boolean add(int pid, String logicalAddr, String mod) throws Exception {
        //System.out.println("IN NRU");
        if (c_instruction == 200) {
            this.clearAllReferenceBit();
        }

        if (list.size() == totalPage) {
            if(mod.equalsIgnoreCase("W"))
                diskWrite++;
            else
                diskRead++;
            //auto replace
            //replace(pid, logicalAddr, mod);
            return false;
        } else {
            String frameNumber = getFrameNumber();
            //System.out.println("DEBUG PRA(ADD)");
            //System.out.println(pid + " " + getPageNumberBit(logicalAddr) + " " + frameNumber);
            //Fields(int pid, String pagdNumber,boolean caching, byte referenced, byte dirtyBit, byte protection, byte present, String frameNumber)
            if (mod.equalsIgnoreCase("W")) {
                Fields f = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 1, (byte) 1, (byte) 2, (byte) 1, frameNumber);
                list.add(f); // NRU 
            } else if (mod.equalsIgnoreCase("R")) {
                Fields f = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 1, (byte) 0, (byte) 2, (byte) 1, frameNumber);
                list.add(f); // NRU 
            }
            c_instruction++;
            return true;
        }
    }
    @Override
    public boolean search(int pid, String addr, String action) throws Exception {
        //System.out.println("IN NRU");
       // System.out.println("Search param :" + addr + " DEC:" + Tools.binaryStringToDecimal(addr));
        addProcess(pid);
        String pageNumber = getPageNumberBit(addr);
        for (Fields fields : list) {
            //System.out.print("compare " + pageNumber + " = " + fields.getPageNumber());
            if (pageNumber.equals(fields.getPageNumber())) {
                //System.out.print(" --FOUND\n");
                if(action.equalsIgnoreCase("W")) {
                    fields.setDirtyBit((byte)1);
                }
                else if(action.equalsIgnoreCase("R")) {
                    fields.setDirtyBit((byte)0);
                }
                
                fields.setReferenced((byte)1);
                return true;
            }
        }
        //System.out.print(" --NOT FOUND\n");

        //Page Fault
        diskRef++;
        pageFault++;
        return false;
    }
    @Override
    public void debugPage() {
        System.out.println("DEBUG Queue");
        for (Fields f : list) {
            System.out.println(f.getPid() + " " + f.getPageNumber() + " " + f.getFrameNumber());
        }
    }
    
    @Override
    protected void clearAllReferenceBit() {
        for (Fields fields : list) {
            fields.setReferenced((byte) 0);    
        }
        c_instruction = 0;
    }
}
