/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

/**
 *
 * @author fahfoii
 */
public class Fields {
    private int pid;
    private String pageNumber;
    private boolean caching;
    private byte referenced;
    private byte dirtyBit;
    private byte protection;
    private byte present;
    //Binary
    private String frameNumber;

    public Fields(int pid, boolean caching, byte referenced, byte dirtyBit, byte protection, byte present, String frameNumber) {
        this.pid = pid;
        this.caching = caching;
        this.referenced = referenced;
        this.dirtyBit = dirtyBit;
        this.protection = protection;
        this.present = present;
        this.frameNumber = frameNumber;
    }

    public Fields(int pid, String pageNumber, boolean caching, byte referenced, byte dirtyBit, byte protection, byte present, String frameNumber) {
        this.pid = pid;
        this.pageNumber = pageNumber;
        this.caching = caching;
        this.referenced = referenced;
        this.dirtyBit = dirtyBit;
        this.protection = protection;
        this.present = present;
        this.frameNumber = frameNumber;
    }

    

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }
    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public boolean isCaching() {
        return caching;
    }

    public void setCaching(boolean caching) {
        this.caching = caching;
    }

    public byte getReferenced() {
        return referenced;
    }

    public void setReferenced(byte referenced) {
        this.referenced = referenced;
    }

    public byte getDirtyBit() {
        return dirtyBit;
    }

    public void setDirtyBit(byte dirtyBit) {
        this.dirtyBit = dirtyBit;
    }

    public byte getProtection() {
        return protection;
    }

    public void setProtection(byte protection) {
        this.protection = protection;
    }

    public byte getPresent() {
        return present;
    }

    public void setPresent(byte present) {
        this.present = present;
    }

    public String getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(String frameNumber) {
        this.frameNumber = frameNumber;
    }
    
    
    
}
