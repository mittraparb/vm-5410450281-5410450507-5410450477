/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

/**
 *
 * @author March
 */
public class FIFO extends PRAlgorithm {

    public FIFO(byte arcBit, int mSize, int pSize, byte rp) throws Exception {
        super(arcBit, mSize, pSize, rp);
    }
   

    @Override
    public void replace(int pid, String logicalAddr, String mod) throws Exception {
        //System.out.println("FIFO REPLACE");
        //local
        if(rp == 0) {
            
        }
        //global
        else {
            Fields temp = (Fields)queue.peek();
            //check dirty bit
            if(temp.getDirtyBit() == 1) {
                diskRef++;
                diskWrite++;
            }
            //System.out.println(getFrameNumber());
            release(temp.getFrameNumber());
            String frameNumber = getFrameNumber();
            //System.out.println("DEBUG FIFO(REPLACE)");
            //System.out.println(pid + " " + getPageNumberBit(logicalAddr) + " " + frameNumber);
            //Fields(int pid, String pagdNumber,boolean caching, byte referenced, byte dirtyBit, byte protection, byte present, String frameNumber)
            
            Fields newFields = null;
            if (mod.equalsIgnoreCase("W")) {
                newFields = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 0, (byte) 1, (byte) 2, (byte) 1, frameNumber);
            } else if (mod.equalsIgnoreCase("R")) {
                newFields = new Fields(pid, getPageNumberBit(logicalAddr), false, (byte) 0, (byte) 0, (byte) 2, (byte) 1, frameNumber);
            }
            //System.out.println(newFields);
            if(newFields == null) System.out.println("Gotcha");
            queue.remove();
            queue.add(newFields);
            
            pageFault++;
            c_instruction++;
        }
    }
}
