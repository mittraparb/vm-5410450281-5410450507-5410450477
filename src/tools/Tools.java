/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import javax.naming.BinaryRefAddr;

/**
 *
 * @author fahfoii
 */
public class Tools {

    public static String getFileExtension(String filename) {
        String extension = "";
        for (int i = filename.length() - 1; i >= 0; i--) {
            if (filename.charAt(i) == '.') {
                break;
            } else {
                extension += filename.charAt(i);
            }
        }
        return new StringBuilder(extension).reverse().toString();
    }

    public static boolean isAcceptableExtension(String extension) {
        //check reverse extension
        if (extension.equalsIgnoreCase("mc") || extension.equalsIgnoreCase("txt")) {
            return true;
        } else {
            return false;
        }
    }

    public static void alertMessage(Object obj) {
        javax.swing.JOptionPane.showMessageDialog(null, obj.toString());
    }

    public static boolean isInteger(String number) {
        return number.matches("\\d+");
    }

    public static String decimalToBinaryString(int dec) {
        return Integer.toBinaryString(dec);
    }

    public static int binaryStringToDecimal(String binary) {
        return Integer.parseInt(binary, 2);
    }

    public static String optimizeBinaryString(int arcBit, String bin) throws Exception {
        String prefix = "";
        int n = arcBit - bin.length();
        if (n < 0) {
            throw new Exception("Overflow : " + bin);
        }
        else if (n == 0){
            return bin;
        }
        
        for (int i = 0; i < n; i++) {
            prefix += '0';
        }
        return prefix + bin;
    }

    private static double logb(double a, double b) {
        return Math.log(a) / Math.log(b);
    }

    public static double log2(double a) {
        return logb(a, 2);
    }
}
