/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * This class make for read code from text file
 * 
 * @author March
 * @version 0.1
 */
public class Reader {
    /** for contain code line by line from file*/
    private List<String> lines;
    public Reader() {
        lines = new LinkedList<String>();
    }
    /**
     * Use this if you want to read code from file.
     * 
     * @exception File not found or corrupt.
     * @param filePath String of directory path file.
     */
    public void readFile(String filePath) throws Exception {
        try {
            clearList();
            lines = Files.readAllLines(Paths.get(filePath), Charset.forName("UTF-8"));
        } catch (IOException ex) {
            throw new Exception(ex.toString());
        }
    }
    
    /**
     * Use this if you want to read code from text area.
     * 
     * @exception String is empty.
     * @param filePath String of directory path file.
     */
    public void readString(String string) throws Exception{
        if (string.equals("") || string == null) {
            throw new Exception("readStrring: parameter is empty string.");
        }
        else {
            clearList();
            Scanner sc = new Scanner(string);
            //append new line to string
            string += "\n";
            for(String s:string.split("\\n")) {
                System.out.println(s);
                lines.add(s);
            }
        }
    }
    /**
     * Call this if you want list of all code line by line after readFile.
     * 
     * @exception Call this before call readFile ,so return <tt>null</tt>
     * @return List of string that contains all code line by line.
     */
    public List<String> getAllLines() {
        return  lines;
    }
    
    /**
     * Call this if you want all code include newline after readFile.
     *     /**
     * @exception Call this before call readFile ,so return <tt>null</tt>
     * @return string that contains all code include newline.
     */
    public String getAllLinesWithNewLine() {
        String temp = "";
        for(String line:lines) {
            temp += line+"\n";
        }
        return temp;
    }
    
    /**
     * Call this before readString or readFile.
     * 
     * clear any data in list
     */
    private void clearList() {
        if(lines != null && !lines.isEmpty()) {
            lines.clear();
        }
    }
}
