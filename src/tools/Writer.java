/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author March
 */
public class Writer {

    public Writer() {
    }
    
    /**
     * Call this if you want to write file.
     * 
     * @param string String to write in your file
     * @param path   Path of direction to place file
     */
    public void writeString(String string,String path) throws Exception {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()));
            writer.write(string);
            writer.close();
        } catch (IOException ex) {
            throw new Exception(ex.toString());
        }
    }
}
